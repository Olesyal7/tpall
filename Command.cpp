#pragma once
#include "Command.h"
#include "Composite.h"
#include "Observer.h"
#include <iostream>
#include <ctime>

const int range = 5;
const int mana = 15;

using namespace std;

void Play::attack(Unit * un1, Unit * un2) {
	un2->be_attacked(un1->get_power());
}
void Play::subs(Unit* un, Observer * obs) {
	obs->subscribe(un);
}
void Play::add(CompositeUnit* main, Unit* un) {
	main->addUnit(un);
}

void Play::hex(Observer * obs) {
	int i = rand() % range + mana;
	obs->get_mana(i);
}

AttackGameCommand::AttackGameCommand(Unit* un1, Unit* un2, Play * pl) {
	player = pl;
	unit1 = un1;
	unit2 = un2;
}

void AttackGameCommand::execute(){
	player->attack(unit1, unit2);
};



SubsGameCommand::SubsGameCommand(Observer * obs, Unit* un, Play * pl) {
	player = pl;
	unit = un;
	observ = obs;
}

void SubsGameCommand::execute() {
	player->subs(unit, observ);
};



HexGameCommand::HexGameCommand(Observer* obs, Play * pl) {
	player = pl;
	observ = obs;
}

void HexGameCommand::execute() {
	player->hex(observ);
};


AddUnitGameCommand::AddUnitGameCommand(CompositeUnit* main, Unit * un, Play * pl) {
	player = pl;
	comp = main;
	unit = un;
}

void AddUnitGameCommand::execute() {
	player->add(comp, unit);
};



AddCompGameCommand::AddCompGameCommand(CompositeUnit* main, Unit * un, Play * pl) {
	player = pl;
	comp = main;
	unit = un;
}

void AddCompGameCommand::execute() {
	player->add(comp, unit);
};




