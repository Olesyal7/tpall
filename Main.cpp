#pragma once
#include "Proxy.h"
#include "Army.h"
#include <iostream>
#include <ctime>
#include "Composite.h"
#include "Command.h"

using namespace std;

int main() {
	srand(time(0));
	setlocale(LC_ALL, "Russian");
	cout << "���� �� ������ ������ �� ��������, ������� 1, ���� �� ������� - ������� 2" << endl;
	int race = 0;
	cin >> race;
	ArmyFactory* f = 0;
	ArmyFactory* g = 0;
	if (race == 1) {
		f = new DragonArmyFactory;
		g = new KnightArmyFactory;
	}
	else if (race == 2) {
		f = new KnightArmyFactory;
		g = new DragonArmyFactory;
	}
	else {
		cout << "������ ������� �����������, ���� ���������";
		return 0;
	}
	Game playerGame(f);
	Game computerGame(g);
	int s = 0;
	int h = 0;
	int m = 0;
	int count = rand() % 5 + 10;
	cout << "������ �� ������ ��������� ���� ��������� �����. ���������� ������ = " << count << endl;
	cout << "������� ��������������� 3 �����, ������ � ����� " << count << endl;
	cout << "���������� ������� ������:" << endl;
	cin >> s;
	cout << "���������� �������� ������:" << endl;
	cin >> h;
	cout << "���������� ��������� ������:" << endl;
	cin >> m;
	CompositeUnit * playerArmy = 0;
	if (s + h + m == count) {
		playerArmy = playerGame.createArmy(s, h, m);
	}
	else {
		cout << "������ ������� �����������, ���� ���������";
		return 0;
	}
	cout << "���� �����" << endl;
	playerArmy->pr();
	count = rand() % 5 + 10;
	s = rand() % count;
	h = rand() % (count - s);
	m = count - s - h;
	CompositeUnit * computerArmy = computerGame.createArmy(s, h, m);
	cout << "����� ����������" << endl;
	computerArmy->pr();
	cout << "� ��� ���� ����������� ��������� 4 ��������� ��������:" << endl;
	cout << "1. ��������� ����������" << endl;
	cout << "2. �������� � ���� ����� ������-�� ����� �� �����" << endl;
	cout << "3. �������� � ���� ����� ��������� ����� �� 1-3 ���������� (�� ����, ��� ��� � 3 ����)" << endl;
	cout << "4. ��������� ����� ����� ������������ �������� ����� ������, ����� � ���� �������� ����" << endl;
	cout << "5. ������������ ������ 1 - 3 ����, ����� �� �������� ���� ����������� ��������(�� ������ ����� �������)" << endl;
	cout << "���� ������������, ���� ��� ����� (���� � ����������) ����" << endl;
	cout << "������ ��� �� ������ �� ������" << endl;
	int com = 0;
	Observer * obs = new Observer();
	vector<Command *> histplayer;
	vector<Command *> histcomp;
	Play * play = new Play;
	while (computerArmy->get_health() > 0 && playerArmy->get_health() > 0) {
		cout << "�������� �������: " << endl;
		cin >> com;
		Command * command = 0;
		if (com == 1) {
			command = new AttackGameCommand(playerArmy, computerArmy, play);
			cout << "�� ������ �� ����������" << endl;
		}
		if (com == 2) {
			cout << "��������: 1 - �������, 2 - ��������, 3 - ���������" << endl;
			int ch = 0;
			cin >> ch;
			Unit * un = playerGame.createUnit(ch);
			command = new AddUnitGameCommand(playerArmy, un, play);
			cout << "� ���� ����� �������� ����� �����" << endl;
		}
		if (com == 3) {
			AddCompGameCommand * b = 0;
			AddCompGameCommand * c = 0;
			if (histplayer.size() > 0) {
				b = dynamic_cast<AddCompGameCommand *>(*prev(histplayer.end()));
			}
			else {
				b = NULL;
			}
			if (histplayer.size() > 1) {
				c = dynamic_cast<AddCompGameCommand *>(*prev(prev(histplayer.end())));
			}
			else {
				c = NULL;
			}
			if (!b && !c) {
				int ch = rand() % 3 + 1;
				int s1 = rand() % ch;
				int h1 = rand() % (ch - s1);
				int m1 = ch - s1 - h1;
				Unit * un = playerGame.createArmy(s1, h1, m1);
				command = new AddCompGameCommand(playerArmy, un, play);
				cout << "� ���� ����� �������� ����� �����" << endl;
			}
			else {
				cout << "�� ��� �����" << endl;
				cout << "��������: 1 - �������, 2 - ��������, 3 - ���������" << endl;
				int ch = 0;
				cin >> ch;
				Unit * un = playerGame.createUnit(ch);
				command = new AddUnitGameCommand(playerArmy, un, play);
				cout << "� ���� ����� �������� ����� �����" << endl;
			}
		}
		if (com == 4) {
			command = new SubsGameCommand(obs, playerArmy, play);
			cout << "�� ���������" << endl;
		}
		if (com == 5) {
			command = new HexGameCommand(obs, play);
			cout << "�� ������� �������� ������" << endl;
		}
		histplayer.push_back(command);
		command->execute();
		cout << "���� �����:" << endl;
		playerArmy->pr();
		cout << "����� ����������:" << endl;
		computerArmy->pr();
		cout << endl;

		if (computerArmy->get_health() > 0) {
			cout << "��� ����������" << endl;
			com = 1 + rand() % 5;
			if (com == 1) {
				command = new AttackGameCommand(computerArmy, playerArmy, play);
				cout << "�� ����� �� ���" << endl;
			}
			if (com == 2) {
				int ch = 1 + rand() % 3;
				Unit * un = computerGame.createUnit(ch);
				command = new AddUnitGameCommand(computerArmy, un, play);
				cout << "� ��� ����� �������� ����� �����" << endl;
			}
			if (com == 3) {
				AddCompGameCommand * b = 0;
				AddCompGameCommand * c = 0;
				if (histcomp.size() > 0) {
					b = dynamic_cast<AddCompGameCommand *>(*prev(histcomp.end()));
				}
				else {
					b = NULL;
				}
				if (histcomp.size() > 1) {
					c = dynamic_cast<AddCompGameCommand *>(*prev(prev(histcomp.end())));
				}
				else {
					c = NULL;
				}
				if (!b && !c) {
					int ch = rand() % 3 + 1;
					int s1 = rand() % ch;
					int h1 = rand() % (ch - s1);
					int m1 = ch - s1 - h1;
					Unit * un = computerGame.createArmy(s1, h1, m1);
					command = new AddCompGameCommand(computerArmy, un, play);
					cout << "� ��� ����� �������� ����� �����" << endl;
				}
				else {
					int ch = rand() % 3 + 1;
					Unit * un = computerGame.createUnit(ch);
					command = new AddUnitGameCommand(computerArmy, un, play);
					cout << "� ��� ����� �������� ����� �����" << endl;
				}
			}
			if (com == 4) {
				command = new SubsGameCommand(obs, computerArmy, play);
				cout << "�� �������� ������" << endl;
			}
			if (com == 5) {
				command = new HexGameCommand(obs, play);
				cout << "�� ������ �������� ������" << endl;
			}
			histcomp.push_back(command);
			command->execute();
			cout << "���� �����:" << endl;
			playerArmy->pr();
			cout << "����� ����������:" << endl;
			computerArmy->pr();
			cout << endl;
		}
	}
	if (computerArmy->get_health() == 0) {
		cout << "You win!" << endl;
	}
	else {
		cout << "You lost" << endl;
	}
	return 0;
}






//int main() {
//	srand(time(0));
//	DragonArmyFactory* drf = new DragonArmyFactory;
//	Game game(drf);
//	CompositeUnit * main = game.createArmy();
//	main->pr();
//	cout << main->get_health();
//	CompositeUnit * also = game.createArmy();
//	also->pr();
//	main->addUnit(also);
//	main->pr();
//}

//int main()
//{
//	Game game;
//	DragonArmyFactory dr_factory;
//	KnightArmyFactory kn_factory;
//	srand(time(0));
//	CompositeUnit * dr = game.createArmy(dr_factory);
//	CompositeUnit * kn = game.createArmy(kn_factory); 
//	cout << "Dragon army: ";
//	dr->pr();
//	cout << endl;
//	cout << "Knight army: ";
//	kn->pr();
//}

/*int main() {
	srand(time(0));
	CompositeUnit* arm = new CompositeUnit;
	for (int i = 0; i < 4; ++i) {
		arm->addUnit(createKnightLegion());
	}
	cout << "Army 1. Power " << arm->get_power() << endl;
	arm->pr();
	srand(time(0));
	CompositeUnit* arm2 = new CompositeUnit;
	for (int i = 0; i < 4; ++i) {
		arm2->addUnit(createDragonLegion());
	}
	cout << "Army 1. Health " << arm2->get_health() << endl;
	arm2->pr();
	Proxy* un = new Proxy(arm2);
	cout << "Army 1 attack Army 2" << endl;
	un->be_attacked(arm->get_power());
	cout << endl;
	cout << "Army 2 after attack. Health " << un->get_health() << endl;
	un->pr();
	cout << "Army 2 attack Army 1" << endl;
	arm->be_attacked(arm2->get_health());
	cout << "Army 1" << endl;
	arm->pr();
	cout << "Now Army 1 tries attack Army 2 again, and if Army 2 is died and our Proxy is working it shows that Army 1 is died" << endl;
	un->be_attacked(arm->get_power());
	delete arm;
	delete arm2;
	return 0;
}*/

