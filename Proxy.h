#pragma once
#include "Unit.h"

class Proxy : public Unit {
public:
	Proxy(Unit* un);
	bool be_attacked(int pow)override;
	int get_power() override;
	int get_health() override;
	bool get_flag() override;
	void pr() override;
	void inc_health(int val) override;
private:
	Unit* unit;
};