#pragma once
#include "Unit.h"

const int SKP = 30;
const int SKH = 400 / 5;

const int HKP = 20;
const int HKH = 500 / 5;

const int MKP = 20;
const int MKH = 400 / 5;

const int SDP = 35;
const int SDH = 350 / 5;

const int HDP = 25;
const int HDH = 450 / 5;

const int MDP = 25;
const int MDH = 350 / 5;

const int range = 5;

using namespace std;

int StrongKnight::get_power() {
	return power;
};

int StrongKnight::get_health() {
		return health;
};

bool StrongKnight::get_flag() {
		return 1;
};

void StrongKnight::pr() {
		cout << "K(-_-)";
};
	
StrongKnight::StrongKnight() {
		power = rand() % range + SKP;
		health = rand() % range + SKH;
};

bool StrongKnight::be_attacked(int pow) {
	if (pow < health) {
		health -= pow;
		return true;
	}
	else {
		health = 0;
		return false;
	}
};

void StrongKnight::inc_health(int val) {
	health += val;
}



int HealthKnight::get_power() {
	return power;
};
int HealthKnight::get_health() {
	return health;
};
bool HealthKnight::get_flag() {
	return 1;
};
void HealthKnight::pr() {
	cout << "K('_')";
};
HealthKnight::HealthKnight() {
	power = rand() % range + HKP;
	health = rand() % range + HKH;
};

bool HealthKnight::be_attacked(int pow) {
	if (pow < health) {
		health -= pow;
		return true;
	}
	else {
		health = 0;
		return false;
	}
};

void HealthKnight::inc_health(int val) {
	health += val;
}


int MagicKnight::get_power() {
	return power;
}
int MagicKnight::get_health() {
	return health;
}
bool MagicKnight::get_flag() {
	return flag;
}
void MagicKnight::pr() {
	cout << "K(0_0)";
}
MagicKnight::MagicKnight() {
	power = rand() % range + MKP;
	health = rand() % range + MKH;
	flag = false;
}
bool MagicKnight::be_attacked(int pow) {
	if (!flag) {
		bool b = rand() % 2;
		if (b) {
			flag = true;
			return false;
		}
		else {
			flag = true;
			be_attacked(pow);
		}
	}
	if (pow < health) {
		health -= pow;
		return true;
	}
	else {
		health = 0;
		return false;
	}
};

void MagicKnight::inc_health(int val) {
	health += val;
}



int StrongDragon::get_power() {
	return power;
}
int StrongDragon::get_health() {
	return health;
}
bool StrongDragon::get_flag() {
	return 1;
}
void StrongDragon::pr() {
	cout << "D(-_-)";
}
StrongDragon::StrongDragon() {
	power = rand() % range + SDP;
	health = rand() % range + SDH;
}
bool StrongDragon::be_attacked(int pow) {
	if (pow < health) {
		health -= pow;
		return true;
	}
	else {
		health = 0;
		return false;
	}
}

void StrongDragon::inc_health(int val) {
	health += val;
}



int HealthDragon::get_power() {
	return power;
}
int HealthDragon::get_health() {
	return health;
}
bool HealthDragon::get_flag() {
	return 1;
}
void HealthDragon::pr() {
	cout << "D('_')";
}
HealthDragon::HealthDragon() {
	power = rand() % range + HDP;
	health = rand() % range + HDH;
}
bool HealthDragon::be_attacked(int pow) {
	if (pow < health) {
		health -= pow;
		return true;
	}
	else {
		health = 0;
		return false;
	}
}

void HealthDragon::inc_health(int val) {
	health += val;
}



int MagicDragon::get_power() {
	return power;
}
int MagicDragon::get_health() {
	return health;
}
bool MagicDragon::get_flag() {
	return flag;
}
void MagicDragon::pr() {
	cout << "D(0_0)";
}
MagicDragon::MagicDragon() {
	power = rand() % range + MDP;
	health = rand() % range + MDH;
	flag = false;
}
bool MagicDragon::be_attacked(int pow) {
	if (!flag) {
		bool b = rand() % 2;
		if (b) {
			flag = true;
			return false;
		}
		else {
			flag = true;
			be_attacked(pow);
		}
	}
	if (pow < health) {
		health -= pow;
		return true;
	}
	else {
		health = 0;
		return false;
	}
}

void MagicDragon::inc_health(int val) {
	health += val;
}

