#pragma once
#include "Observer.h"
#include "Composite.h"
#include "UNIT.h"
class Play {
public:
	void attack(Unit* un1, Unit* un2);
	void subs(Unit* un, Observer* Obs);
	void add(CompositeUnit* main, Unit* un);
	void hex(Observer * obs);
};

class Command {
public:
	virtual ~Command() {}
	virtual void execute() = 0;
};

class AttackGameCommand : public Command {
public:
	AttackGameCommand(Unit* un1, Unit* un2, Play * pl);
	void execute() override;
private:
	Unit * unit1;
	Unit * unit2;
	Play * player;
};

class SubsGameCommand : public Command {
public:
	SubsGameCommand(Observer* obs, Unit* un, Play * pl);
	void execute() override;
private:
	Observer * observ;
	Unit * unit;
	Play * player;
};

class HexGameCommand : public Command {
public:
	HexGameCommand(Observer* obs, Play * pl);
	void execute() override;
private:
	Observer * observ;
	Play * player;
};

class AddUnitGameCommand : public Command {
public:
	AddUnitGameCommand(CompositeUnit* main, Unit * un, Play * pl);
	void execute() override;
private:
	CompositeUnit * comp;
	Unit * unit;
	Play * player;
};

class AddCompGameCommand : public Command {
public:
	AddCompGameCommand(CompositeUnit* main, Unit * un, Play * pl);
	void execute() override;
private:
	CompositeUnit * comp;
	Unit * unit;
	Play * player;
};